﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Actions : EditorWindow
{
    List<string> labels;

    [MenuItem("Window/Actions")]
    static void Init()
    {
        GetWindow<Actions>("Actions");
        
    }

    private void OnEnable()
    {
        labels = new List<string>( AssetDatabase.FindAssets("t:GActionObject", new[] { "Assets/Game/Data" }));
        
    }

    private void OnGUI()
    {
        foreach (var item in labels)
        {
            Debug.Log(item);
            GUILayout.Label(item, EditorStyles.boldLabel);
        }
    }


}
