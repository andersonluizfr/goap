﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Goap;
using System.Linq;

namespace AStar
{
    public static class GAStar
    {
        public static int maxOfIterations = 1000;

        public static GNode<T> Planning<T>(GWorldState startState, List<GAction> actions)
        {
            Debug.Log("planning");

            GNode<T> startNode = new GNode<T>
            {
                worldState = startState
            };

            HashSet<GNode<T>> closedSet = new HashSet<GNode<T>>();
            GHeap<GNode<T>> openSet = new GHeap<GNode<T>>(1000);

            openSet.Add(startNode);
            int cnt = 0;
            System.Text.StringBuilder s = new System.Text.StringBuilder();

            while (openSet.Count > 0 && cnt < maxOfIterations)
            {
                GNode<T> currentNode = openSet.RemoveFirstItem();

                closedSet.Add(currentNode);
                if (currentNode.IsGoalState())
                    return currentNode;


                //if (openSet.Count > 0)
                //{
                //    foreach(var t in openSet.items.Where(i => i!=null).ToList())
                //    {

                //        if(t!= null && t.worldState.action !=null)
                //            s.AppendFormat(" {0} ", t.worldState.action.actionName);
                //    }
                //}


                s.Append("[");
                List<GWorldState> worldStates = GetAvailableNodes(currentNode.worldState, actions);

                foreach (GWorldState ws in worldStates)
                {

                    GNode<T> nNode = new GNode<T>
                    {
                        worldState = ws
                    };
                    int cost = currentNode.GCost + ws.action.cost;
                    if (cost < nNode.GCost || !openSet.Contains(nNode))
                    {
                        nNode.GCost = cost;
                        nNode.HCost = ws.State.Where(entry => entry.Value.Key != entry.Value.Value).ToList().Count;
                        nNode.Parent = currentNode;

                        if (!openSet.Contains(nNode))
                        {
                            openSet.Add(nNode);
                            Debug.Log(string.Format("added {0}", ws.action.actionName));
                        }
                        else
                        {
                            openSet.UpdateItem(nNode);
                            Debug.Log(string.Format("updated {0}", ws.action.actionName));
                        }
                    }
                    //s.AppendFormat(" {0} {1}", ws.action.actionName, nNode.FCost);

                }
                s.Append("]");
                //Debug.Log(s.ToString());
                s.Clear();
                cnt++;
            }

            return null;
        }

        public static List<GWorldState> GetAvailableNodes(GWorldState currentWS, List<GAction> actions)
        {

            List<GWorldState> availableWs = new List<GWorldState>();
            List<GEGoalKey> conditionsToFullfil = currentWS.GetGoalKeyList();
            foreach (var cond in conditionsToFullfil)
            {
                foreach (var action in actions)
                {
                    if (action.effects.ContainsKey(cond))
                    {
                        currentWS.State[cond] = new KeyValuePair<bool, bool>(currentWS.State[cond].Value, currentWS.State[cond].Value);
                        GWorldState newWs = new GWorldState(currentWS.State);
                        foreach (var pre in action.preConditions)
                        {
                            if (pre.Key != GEGoalKey.KNone)
                                newWs.AddPrecondition(pre.Key, new KeyValuePair<bool, bool>(!pre.Value, pre.Value));
                        }

                        newWs.action = action;
                        availableWs.Add(newWs);

                    }
                }
            }

            return availableWs;
        }
    }

}
