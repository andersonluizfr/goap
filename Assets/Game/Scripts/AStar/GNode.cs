﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Goap;
using System.Linq;

namespace AStar
{
    public class GNode<T> : IHeapItem<GNode<T>>
    {
        private int gCost;
        private int hCost;
        private GNode<T> parent;
        public GAction action;
        private int heapIndex;
        public int GCost { get => gCost; set => gCost = value; }

        public int HCost { get => hCost; set => hCost = value; }

        public int FCost { get => gCost + HCost; }

        public int HeapIndex { get => heapIndex; set => heapIndex = value; }


        public GNode<T> Parent { get => parent; set => parent = value; }

        public GWorldState worldState;

        public GNode()
        {

        }

        public void CalcHcost(IHeapItem<GNode<T>> item)
        {
            int x = item.HeapIndex + 2;
        }



        public bool IsGoalState()
        {
            return worldState.State.Where(entry => entry.Value.Key != entry.Value.Value).Count() == 0;
        }

        public int CompareTo(GNode<T> other)
        {
            int compare = FCost.CompareTo(other.FCost);
            if (compare == 0)
            {
                compare = hCost.CompareTo(other.hCost);
            }

            return -compare;
        }
    }
}
