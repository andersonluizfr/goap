﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public List<GameObject> trees;
    [HideInInspector]
    public List<GameObject> mines;
    // Start is called before the first frame update

    public static GameManager instance;

    private void Awake()
    {
        instance = this;
        trees = new List<GameObject>(GameObject.FindGameObjectsWithTag("Tree"));
        mines = new List<GameObject>(GameObject.FindGameObjectsWithTag("Mine"));
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
