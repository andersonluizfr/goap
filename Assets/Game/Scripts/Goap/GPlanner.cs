﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using AStar;
using Goap.Actions;

namespace Goap
{
    public class GPlanner : MonoBehaviour
    {
       
        private Queue<PlanRequest> planRequestQueue = new Queue<PlanRequest>();
        private PlanRequest currentPlanRequest;
        private Agent currentAgent;
        private static GPlanner instance;
        public bool isProcessingPlan;

        public static GPlanner Instance
        {
            get => instance;
        }


        private void Awake()
        {
            if (instance == null)
                instance = this;
        }

        // Start is called before the first frame update
        void Start()
        {
            //gActions = new List<GAction>()
            //{
            //    new GACutTree("Cut Tree",1),
            //    new GATakeAxe("Take Axe",10),
            //    new GAGetLoggs("Get Loggss",4),
            //    new GASellLoggs("Sell Loggs",2),

            //};

            //CalculateActionSequence();
        }
        
        public static void RequestPlan(Agent agent, GWorldState goalWorldState, Action<Queue<GAction>, bool> callback)
        {
            PlanRequest newRequest = new PlanRequest(goalWorldState, agent, callback);
            Instance.planRequestQueue.Enqueue(newRequest);
            Instance.TryProcessNext();
        }
        
        void TryProcessNext()
        {
            if(!isProcessingPlan && planRequestQueue.Count > 0)
            {
                currentPlanRequest = planRequestQueue.Dequeue();
                currentAgent = currentPlanRequest.asker;
                isProcessingPlan = true;

                List<GAction> possibleActions = new List<GAction>();
                foreach (var action in currentAgent.agentActions)
                {
                    if (action.CheckProceduralPreCondition(currentAgent))
                        possibleActions.Add(action);
                }
                
                GNode<bool> resultNode = GAStar.Planning<bool>(currentPlanRequest.goalWorldState, possibleActions);
                if(resultNode != null)
                {
                    Queue<GAction> gActions = new Queue<GAction>();
                    CreateSequence(gActions, resultNode);
                    OnFinishedProcessingPlan(gActions,true);
                }else
                    OnFinishedProcessingPlan(null, false);

            }
        }

        public void CreateSequence(Queue<GAction> gActions, GNode<bool> result)
        {
            GNode<bool> current = result;
            while (current != null)
            {
                if (current.worldState.action != null)
                    gActions.Enqueue(current.worldState.action);

                current = current.Parent;
            }
        }

        public void OnFinishedProcessingPlan(Queue<GAction> actions, bool success)
        {
            Debug.Log("Finished processing plan");
            currentPlanRequest.Callback(actions, success);
            Debug.Log("try process next");
            isProcessingPlan = false;
            currentAgent = null;
            TryProcessNext();
        }

        public List<GAction> DebugSequence<T>(GNode<T> gNode)
        {
            List<GAction> gActions = new List<GAction>();
            GNode<T> current = gNode;
            while (current != null)
            {
                if (current.worldState.action != null)
                    gActions.Add(current.worldState.action);

                current = current.Parent;
            }
            return gActions;
        }

       
    }


    public struct PlanRequest
    {
        public GWorldState goalWorldState;
        public Agent asker;
        private Action<Queue<GAction>, bool> callback;
        public Action<Queue<GAction>, bool> Callback { get => callback; }

        public PlanRequest(GWorldState goalWorldState, Agent asker,Action<Queue<GAction>,bool> callback)
        {
            Debug.Log("New Plan Request");
            this.goalWorldState = goalWorldState;
            this.callback = callback;
            this.asker = asker;
        }

       
    }
}
