﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Goap.Actions
{
    public class GACutTree : GAction
    {
        public GACutTree(string actionName, int cost) : base(actionName, cost)
        {
            AddPreCondition(GEGoalKey.KhasAxe, true);
            AddEffect(GEGoalKey.KCutTree, true);
            base.requireInRange = true;
            range = 5.0f;
        }
        
        public override void Perform()
        {
            Debug.Log(string.Format("Perform action {0}", actionName));
            target.SetActive(false);
            target = null;
            onFinishAction?.Invoke(true);
        }
        public override bool IsDone() { return isDone; }

        public override bool IsInRange(Agent agent)
        {
            if(target != null)
            {
                float distance = Vector3.Distance(agent.transform.position, target.transform.position);
                
                return distance <= range;
            }

            return false;
            
        }

        public override bool CheckProceduralPreCondition(Agent agent)
        {
            float minDistance = float.PositiveInfinity;
            foreach(var tree in GameManager.instance.trees)
            {
                float checkDistance = Vector3.Distance(agent.transform.position, tree.transform.position);
                if (checkDistance < minDistance)
                {
                    minDistance = checkDistance;
                    target = tree;
                }
            }

            if (target != null)
                return true;
            else
                return false;
        }

    }

    public class GATakeAxe : GAction
    {
        public GATakeAxe(string actionName, int cost) : base(actionName, cost)
        {
            AddEffect(GEGoalKey.KhasAxe, true);
        }

        public override bool IsInRange(Agent agent)
        {
            return true;
        }
        public override void Perform() {
            onFinishAction?.Invoke(true);
        }
        public override bool IsDone() { return isDone; }
        public override bool CheckProceduralPreCondition(Agent agent)
        {
            return true;
        }
    }

    public class GAGetLoggs : GAction
    {
        public GAGetLoggs(string actionName, int cost) : base(actionName, cost)
        {
            AddPreCondition(GEGoalKey.KCutTree, true);
            AddEffect(GEGoalKey.KLoggs, true);
        }

        public override bool IsInRange(Agent agent)
        {
            return true;
        }
        public override void Perform() { }
        public override bool IsDone() { return false; }
        public override bool CheckProceduralPreCondition(Agent agent)
        {
            return true;
        }
    }

    public class GASellLoggs : GAction
    {
        public GASellLoggs(string actionName, int cost) : base(actionName, cost)
        {
            AddPreCondition(GEGoalKey.KLoggs, true);
            AddEffect(GEGoalKey.KGetMoney, true);
        }

        public override void Perform() { }
        public override bool IsDone() { return false; }
        public override bool IsInRange(Agent agent)
        {
            return true;
        }
        public override bool CheckProceduralPreCondition(Agent agent)
        {
            return true;
        }
    }

    public class GASellAxe : GAction
    {
        public GASellAxe(string actionName, int cost) : base(actionName, cost)
        {
            AddPreCondition(GEGoalKey.KhasAxe, true);
            AddEffect(GEGoalKey.KGetMoney, true);
        }

        public override void Perform() { }
        public override bool IsDone() { return false; }
        public override bool IsInRange(Agent agent)
        {
            return true;
        }
        public override bool CheckProceduralPreCondition(Agent agent)
        {
            return true;
        }
    }

}
