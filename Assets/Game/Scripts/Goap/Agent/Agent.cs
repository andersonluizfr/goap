﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;
using FSM;
using Goap.Actions;

public class Agent : MonoBehaviour
{

    private FSM<Agent> fsm;
    private NavMeshAgent nav;
    private Queue<GAction> actionQueue;
    [HideInInspector]
    public List<GAction> agentActions;
    public Action onAction;
    

    public Queue<GAction> ActionQueue { get => actionQueue; set => actionQueue = value; }

    private void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        agentActions = new List<GAction>()
        {
                new GACutTree("Cut Tree",1),
                new GATakeAxe("Take Axe",10),
                new GAGetLoggs("Get Loggss",4),
                new GASellLoggs("Sell Loggs",2),
        };
        ActionQueue = new Queue<GAction>(10);
        fsm = new Agent_StateMachine(this);
        fsm.Start();
        
    }

    // Update is called once per frame
    void Update()
    {
        fsm.Update();
    }

    public FSM<Agent> GetFSM()
    {
        return fsm;
    }
    
    public bool HasPlan()
    {
        return ActionQueue.Count > 0;
    }

    public void Move(Vector3 dest)
    {
        nav.SetDestination(dest);
    }

    public bool HasReachedDestination()
    {
        return nav.remainingDistance <= 5.0f;
    }
}


