﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;
using Goap;

public class Agent_StateMachine : FSM<Agent>
{
    private IdleState idle;
    private MoveTo move;
    private PerformAction doAction;

    public Agent_StateMachine(Agent owner) : base(owner)
    {
        idle = new IdleState(this);
        move = new MoveTo(this);
        doAction = new PerformAction(this);
    }

    public override void ChangeState(State<Agent> oldState, State<Agent> newState)
    {
        base.ChangeState(oldState, newState);
        if (oldState != null)
            oldState.End();

        if (oldState != newState)
        {
            Current = newState;
            Current.Begin();
        }
    }

    public override void Start()
    {
        Current = idle;
        Current.Begin();
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public IdleState GetIdleState()
    {
        return idle;
    }

    public MoveTo GetMoveToState()
    {
        return move;
    }

    public PerformAction GetPerformActionState()
    {
        return doAction;
    }
}

public class IdleState : State<Agent>
{
    public IdleState(FSM<Agent> parent) : base(parent) { }

    public override void Begin()
    {
        base.Begin();
        //TODO 
        /*Defining the basic needs for the agent to start a planning */
        GPlanner.RequestPlan(parent.Owner, new GWorldState(GEGoalKey.KCutTree, true), OnRequestFinished);
    }

    public void OnRequestFinished(Queue<GAction> actions, bool success)
    {
        Debug.Log("Request Finished");
        if (success)
        {
            Debug.Log("Plan Success");
            parent.Owner.ActionQueue = actions;
            parent.ChangeState(this, (parent as Agent_StateMachine).GetPerformActionState());
        }
        else
        {
            Debug.Log("Plan Failed");
        }
    }

    public override void End()
    {
        base.End();
        Debug.Log("End Idle");

    }

    public override void Update()
    {
        base.Update();
    }
}


public class MoveTo : State<Agent>
{
    private GAction currentAction;
    private bool hasReached;
    public MoveTo(FSM<Agent> parent) : base(parent) { }

    public override void Begin()
    {
        base.Begin();
        Debug.Log("Moving to target");
        hasReached = false;

        currentAction = parent.Owner.ActionQueue.Peek();
        parent.Owner.Move(currentAction.target.transform.position);

    }

    void OnReachedDestination()
    {
        if (parent.Owner.HasReachedDestination())
        {
            hasReached = true;
            Debug.Log("Reached Destination");
            parent.ChangeState(this, (parent as Agent_StateMachine).GetPerformActionState());
        }
    }

    public override void End()
    {
        base.End();
    }

    public override void Update()
    {
        base.Update();
        if (!hasReached)
            OnReachedDestination();
    }
}

public class PerformAction : State<Agent>
{
    GAction currentAction;

    public PerformAction(FSM<Agent> parent) : base(parent) { }

    public override void Begin()
    {
        base.Begin();

        if (parent.Owner.HasPlan())
        {
            currentAction = parent.Owner.ActionQueue.Peek();

            bool isInRange = currentAction.requireInRange ? currentAction.IsInRange(parent.Owner) : true;
            if (isInRange)
            {
                currentAction.onFinishAction = OnActionPerformed;
                currentAction.Perform();
            }
            else
            {
                parent.ChangeState(this, (parent as Agent_StateMachine).GetMoveToState());
            }
        }
        else
        {
            //parent.ChangeState(this, (parent as Agent_StateMachine).GetIdleState());
        }

    }

    void OnActionPerformed(bool success)
    {
        if (success)
        {
            currentAction.onFinishAction = null;
            parent.Owner.ActionQueue.Dequeue();
            Begin();
        }
    }

    public override void End()
    {
        base.End();
        Debug.Log("End of Perform Action");
    }

    public override void Update()
    {
        base.Update();
    }
}