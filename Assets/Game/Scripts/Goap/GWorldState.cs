﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace Goap
{
    public enum GEGoalKey
    {
        KNone,
        KTargetIsDead,
        KCutTree,
        KLoggs,
        KGetAxe,
        KhasAxe,
        KEatPie,
        KMakePie,
        KGetApple,
        KGetMoney,
        KUnarmed,
        KTakeLoggs,
        KTargerInRange

    }

    public class GWorldState
    {
        private Dictionary<GEGoalKey, KeyValuePair<bool, bool>> state;

        public GAction action;

        public GWorldState(GEGoalKey goal, bool gValue)
        {
            state = new Dictionary<GEGoalKey, KeyValuePair<bool, bool>>();
            state.Add(goal, new KeyValuePair<bool, bool>(!gValue, gValue));

        }

        public GWorldState(Dictionary<GEGoalKey, KeyValuePair<bool, bool>> currentState)
        {
            state = new Dictionary<GEGoalKey, KeyValuePair<bool, bool>>(currentState);
        }

        public Dictionary<GEGoalKey, KeyValuePair<bool, bool>> State { get => state; }

        public void AddPrecondition(GEGoalKey key, KeyValuePair<bool, bool> value)
        {
            if (state.Keys.Contains(key))
            {
                state[key] = new KeyValuePair<bool, bool>(value.Value, value.Value);
            }
            else
            {
                state.Add(key, value);
            }

        }

        public List<GEGoalKey> GetGoalKeyList()
        {
            return state.Where(entry => entry.Value.Key != entry.Value.Value).Select(entry => entry.Key).ToList();

        }
    }

}
