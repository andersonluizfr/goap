﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Goap;

public static class Pairing 
{
    public static KeyValuePair<GEGoalKey, bool> Of(GEGoalKey key, bool value)
    {
        return new KeyValuePair<GEGoalKey, bool>(key, value);
    }
}
