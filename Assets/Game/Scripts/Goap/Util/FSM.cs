﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FSM
{
    public abstract class State<T>
    {
        protected FSM<T> parent;

        public State ( FSM<T> parent)
        {
            this.parent = parent;
        }

        public virtual void Begin() { }

        public virtual void End() { }

        public virtual void Update() { }

    }

    public class FSM<T>
    {
        private T owner;

        public T Owner { get=> owner; }

        public FSM(T owner)
        {
            this.owner = owner;
        }

        public List<State<T>> cache;

        private State<T> current;

        public State<T> Current { get => current; set => current = value; }

        public virtual void ChangeState(State<T> oldState, State<T> newState)
        {

        }

        public virtual void Start()
        {

        }

        public virtual void Update()
        {
            current.Update();
        }
    }

}
