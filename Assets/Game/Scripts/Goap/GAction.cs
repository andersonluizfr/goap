﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Goap;

public abstract class GAction
{
    public Dictionary<GEGoalKey, bool> preConditions;
    public Dictionary<GEGoalKey, bool> effects;
    public string actionName;
    public int cost;
    public bool requireInRange;
    public Action<bool> onFinishAction;
    public GameObject target;

    protected bool isDone;
    protected float range;

    public GAction(string actionName, int cost)
    {
        this.actionName = actionName;
        this.cost = cost;
        preConditions = new Dictionary<GEGoalKey, bool>();
        effects = new Dictionary<GEGoalKey, bool>();
    }

    public void AddPreCondition(GEGoalKey key, bool value)
    {
        preConditions.Add(key, value);
    }
    public void AddEffect(GEGoalKey key, bool value)
    {
        effects.Add(key, value);
    }

    public abstract bool IsInRange(Agent agent);
    public abstract void Perform();
    public abstract bool IsDone();
    public abstract bool CheckProceduralPreCondition(Agent agent);



}
