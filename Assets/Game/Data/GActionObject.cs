﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Goap;


[CreateAssetMenu(fileName = "Data", menuName = "Data/Actions", order = 1)]
public class GActionObject : ScriptableObject
{
    public string actionName = "NewActionObjet";
    public int cost = 0;
    public bool requireInRange = false;
    public List<PairClass> preconditions;
    public List<PairClass> effects;
}

[System.Serializable]
public class PairClass
{
    public GEGoalKey key;
    public bool value;
}